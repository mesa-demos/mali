/*
 * Copyright (C) 2010 LunarG Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Authors:
 *    Chia-I Wu <olv@lunarg.com>
 */

#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#define EGL_EGLEXT_PROTOTYPES
#include "EGL/egl.h"
#include "EGL/eglext.h"

#include "eglutint.h"

#define MAX_MODES 100

#ifdef EGL_SCREEN_BIT_MESA
static EGLScreenMESA kms_screen;
static EGLModeMESA kms_mode;
#endif
static EGLint kms_width, kms_height;

void
_eglutNativeInitDisplay(void)
{
   _eglut->native_dpy = EGL_DEFAULT_DISPLAY;
#ifdef EGL_SCREEN_BIT_MESA
   _eglut->surface_type = EGL_SCREEN_BIT_MESA;
#else
   _eglut->surface_type = EGLUT_NATIVE_SURFACE;
#endif
}

void
_eglutNativeFiniDisplay(void)
{
#ifdef EGL_SCREEN_BIT_MESA
   kms_screen = 0;
   kms_mode = 0;
#endif
   kms_width = 0;
   kms_height = 0;
}

static void
init_kms(void)
{
#ifdef EGL_SCREEN_BIT_MESA
   EGLModeMESA modes[MAX_MODES];
   EGLint num_screens, num_modes;
   EGLint width, height, best_mode;
   EGLint i;
   const char *exts;

   exts = eglQueryString(_eglut->dpy, EGL_EXTENSIONS);
   if (!exts || !strstr(exts, "EGL_MESA_screen_surface"))
      _eglutFatal("EGL_MESA_screen_surface is not supported\n");

   if (!eglGetScreensMESA(_eglut->dpy, &kms_screen, 1, &num_screens) ||
         !num_screens)
      _eglutFatal("eglGetScreensMESA failed\n");

   if (!eglGetModesMESA(_eglut->dpy, kms_screen,
            modes, MAX_MODES, &num_modes) || !num_modes)
      _eglutFatal("eglGetModesMESA failed!\n");

   printf("Found %d modes:\n", num_modes);

   best_mode = 0;
   width = 0;
   height = 0;
   for (i = 0; i < num_modes; i++) {
      EGLint w, h;
      eglGetModeAttribMESA(_eglut->dpy, modes[i], EGL_WIDTH, &w);
      eglGetModeAttribMESA(_eglut->dpy, modes[i], EGL_HEIGHT, &h);
      printf("%3d: %d x %d\n", i, w, h);
      if (w > width && h > height) {
         width = w;
         height = h;
         best_mode = i;
      }
   }


   kms_mode = modes[best_mode];
   kms_width = width;
   kms_height = height;
#else
   /* just pick arbitrary size when Mesa extensions not present */
   kms_width = 640;
   kms_height = 480;
#endif
   printf("Will use screen size: %d x %d\n", kms_width, kms_height);
}

void
_eglutNativeInitWindow(struct eglut_window *win, const char *title,
                       int x, int y, int w, int h)
{
   EGLint surf_attribs[16];
   EGLint i=0;

   init_kms();

#ifdef EGL_SCREEN_BIT_MESA
   surf_attribs[i++] = EGL_WIDTH;
   surf_attribs[i++] = kms_width;
   surf_attribs[i++] = EGL_HEIGHT;
   surf_attribs[i++] = kms_height;
   surf_attribs[i++] = EGL_NONE;

   /* create surface */
   win->native.u.surface = eglCreateScreenSurfaceMESA(_eglut->dpy,
         win->config, surf_attribs);
   if (win->native.u.surface == EGL_NO_SURFACE)
      _eglutFatal("eglCreateScreenSurfaceMESA failed\n");

   if (!eglShowScreenSurfaceMESA(_eglut->dpy, kms_screen,
            win->native.u.surface, kms_mode))
      _eglutFatal("eglShowScreenSurfaceMESA failed\n");

   win->native.width = kms_width;
   win->native.height = kms_height;
#elif defined(EGL_MALI)

   surf_attribs[i++] = EGL_NONE;
     {
       struct mali_native_window native_window = { kms_width, kms_height };
       win->native.u.surface = eglCreateWindowSurface(_eglut->dpy, win->config, &native_window, surf_attribs);
     }

   if(!eglQuerySurface(_eglut->dpy, win->native.u.surface, EGL_WIDTH, &win->native.width) ||
      !eglQuerySurface(_eglut->dpy, win->native.u.surface, EGL_WIDTH, &win->native.height))
      _eglutFatal("eglQuerySurfacex` failed\n");

#else
#error EGL implementation not supported.
#endif
}

void
_eglutNativeFiniWindow(struct eglut_window *win)
{
#ifdef EGL_SCREEN_BIT_MESA
   eglShowScreenSurfaceMESA(_eglut->dpy,
         kms_screen, EGL_NO_SURFACE, 0);
#endif
   eglDestroySurface(_eglut->dpy, win->native.u.surface);
}

void
_eglutNativeEventLoop(void)
{
   int start = _eglutNow();
   int frames = 0;

   _eglut->redisplay = 1;

   while (1) {
      struct eglut_window *win = _eglut->current;
      int now = _eglutNow();

      if (now - start > 5000) {
         double elapsed = (double) (now - start) / 1000.0;

         printf("%d frames in %3.1f seconds = %6.3f FPS\n",
               frames, elapsed, frames / elapsed);
         fflush(stdout);

         start = now;
         frames = 0;

         /* send escape */
         if (win->keyboard_cb)
            win->keyboard_cb(27);
      }

      if (_eglut->idle_cb)
         _eglut->idle_cb();

      if (_eglut->redisplay) {
         _eglut->redisplay = 0;

         if (win->display_cb)
            win->display_cb();
         eglSwapBuffers(_eglut->dpy, win->surface);
         frames++;
      }
   }
}
